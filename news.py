import requests
import pprint
import datetime
import smtplib
import os
from email.message import EmailMessage
e_pw = os.environ.get('E_PW')
e_user = os.environ.get('E_USER')
news_articles = []

today = datetime.date.today()
yesterday = today - datetime.timedelta(days=1)
payload = {
    'apiKey': '6c7ae9f99cc8450c87d64834e5d880eb',
    'q': 'cryptocurrency',
    'from': yesterday,
    'to:': today,
    'pageSize': 5
}

#Retrieves the data from the API and appends the news_article list with the title of the returned data as well as the url.
def news_requests():
    url = requests.get('http://newsapi.org/v2/everything', params=payload)
    j = url.json()
    for items in j['articles']:
        print('\n\n')
        news_articles.append(f'{items["title"]}: {items["url"]}')

#Sends an SMTP 
def sending_mail():
    with smtplib.SMTP('smtp-mail.outlook.com', 587) as smtp:
        smtp.ehlo()
        smtp.starttls()
        smtp.ehlo()
        smtp.login(e_user, e_pw)
        subject = 'news'
        body = f'{news_articles}'
        msg = f'Subject: {subject} \n\n{news_articles}'
        smtp.sendmail(e_user, e_user, msg.encode('utf-8'))

try:
    news_requests()
    sending_mail()
    print('Mail Sent!')
except:
    print('Error')